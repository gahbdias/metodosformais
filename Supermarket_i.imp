/* Supermarket_i
 * Author: samuel.lemos.104
 * Creation date: 13/07/2022
 */

IMPLEMENTATION Supermarket_i
REFINES Supermarket

VALUES PRODUTOS = 0..9;
                  CARRINHOS = 0..9;
                  PESSOAS = 0..9

CONCRETE_VARIABLES         carrinho_r, produto_r, preco_r, estoque_r, checkin_r, fidelidade_r, clientes_r, total_r, fila_r, nota_r, cashback_r, hwm_fila, alternative_arr, prod_alt_arr

INVARIANT   hwm_fila : NATURAL & 
            alternative_arr : 0..card(PESSOAS)-1 --> PESSOAS &
            prod_alt_arr : 0..card(PRODUTOS)-1 --> PRODUTOS &
  
            clientes_r: PESSOAS --> BOOL & dom(clientes_r |> {TRUE}) = clientes &
            
            produto_r: PRODUTOS --> BOOL & dom(produto_r |> {TRUE}) = produto &
            
            fidelidade_r: PESSOAS --> BOOL & dom(fidelidade_r |> {TRUE}) = fidelidade & 
            
            total_r: PESSOAS --> INTEGER & !cc.(cc:clientes => total(cc) = total_r(cc)) &
            
            preco_r: PRODUTOS --> NATURAL & !pp.(pp:produto => preco(pp) = preco_r(pp)) &
            
            estoque_r: PRODUTOS --> NATURAL & !pp.(pp:produto => estoque(pp) = estoque_r(pp)) &
            
            checkin_r: PESSOAS --> BOOL & dom(checkin_r |> {TRUE}) = dom(checkin |> {TRUE}) &
            
            fila_r: 0..hwm_fila-1 --> PESSOAS & !pp.(pp:ran(fila) => pp:(ran(fila_r))) & hwm_fila <= card(fila) &
            
            nota_r: PESSOAS --> BOOL & dom(nota_r |> {TRUE}) = dom(nota |> {TRUE}) & 
            
            cashback_r: PESSOAS --> NATURAL & !pp.(pp:fidelidade => cashback(pp) = cashback_r(pp)) &
            
            carrinho_r : PESSOAS * PRODUTOS --> NATURAL & !cc.(cc:clientes => !pp.(pp:produto => carrinho(cc)(pp) = carrinho_r(cc,pp)))

INITIALISATION      carrinho_r := PESSOAS*PRODUTOS * {0};
                    produto_r := PRODUTOS * {FALSE}; 
                    preco_r := PRODUTOS * {0}; 
                    estoque_r := PRODUTOS * {0}; 
                    checkin_r := PESSOAS * {FALSE}; 
                    fidelidade_r := PESSOAS * {FALSE}; 
                    clientes_r := PESSOAS*{FALSE}; 
                    total_r := PESSOAS * {0}; 
                    fila_r := {}; 
                    nota_r := PESSOAS * {FALSE}; 
                    cashback_r := PESSOAS*{0};
                    hwm_fila := 0;
                    alternative_arr := (0..card(PESSOAS)-1)*PESSOAS;
                    prod_alt_arr := (0..card(PRODUTOS)-1)*PRODUTOS

OPERATIONS
  addProdCarrinho(cc, pp, qq) =
    carrinho_r(cc,pp) := carrinho_r(cc,pp)+qq;


  removeProdCarrinho(cc, pp, qq) =
    carrinho_r(cc,pp) := carrinho_r(cc,pp)-qq;


  addProdutoEstoque(pp, pc, qq) =
  BEGIN
    estoque_r(pp) := qq;
    preco_r(pp) := pc;
    produto_r(pp) := TRUE
  END;


  alterarQuantidadeEstoque(pp, qq) =
    estoque_r(pp) := qq;


  alterarPrecoProduto(pp, pc) =
    preco_r(pp) := pc;


  removerProdutoEstoque(pp) =
  BEGIN
    estoque_r(pp) := 0;
    produto_r(pp) := FALSE;
    VAR ii IN
      ii := card(alternative_arr);
      WHILE ii > 0 
      DO
        VAR ps IN
          ii := ii - 1;
          ps := alternative_arr(ii);
          carrinho_r(ps,pp) := 0
        END
        INVARIANT ii : NAT & ii-1 : dom(alternative_arr)
        VARIANT ii
      END
     END
  END;


  cadastrarCliente(cc) =
    clientes_r(cc) := TRUE;


  removerCliente(cc) =
    clientes_r(cc) := FALSE;


  checkinCliente(cc) =
    checkin_r(cc) := TRUE;


  checkoutCliente(cc) =
    checkin_r(cc) := FALSE;


  fidelizarCliente(cc) =
  BEGIN
    fidelidade_r(cc) := TRUE;
    cashback_r(cc) := 0
  END;


  desfidelizarCliente(cc) =
    fidelidade_r(cc) := FALSE;


  qt, pc, sum, tt <-- emitirNota(cc) =
  BEGIN
    nota_r(cc) := TRUE;
    VAR ii, pd, qtcar IN
      ii := card(produto_r);
      qtcar := 0;
      WHILE ii > 0 
      DO
        ii := ii - 1;
        pd := prod_alt_arr(ii);
        qtcar := carrinho_r(cc,pd);
        IF qtcar > 0 
        THEN 
          qt(pd) := carrinho_r(cc,pd);
          pc(pd) := preco_r(pd);
          sum(pd) := carrinho_r(cc,pd)*preco_r(pd)
        END
        INVARIANT ii : NAT
        VARIANT ii
      END
     END;
    tt := total_r(cc)
  END;


  xx <-- finalizarCompra(cc) =
  BEGIN
    xx := total_r(cc);
    checkin_r(cc) := FALSE;
    total_r(cc) := 0;
    nota_r(cc) := FALSE;
    fila_r := tail(fila_r);
    hwm_fila := hwm_fila-1; //operando a fila
    IF fidelidade_r(cc) = TRUE THEN cashback_r(cc) := 1 END;
    VAR ii, pd IN
      ii := card(produto_r);
      WHILE ii > 0 
      DO
        ii := ii - 1;
        pd := prod_alt_arr(ii);
        carrinho_r(cc,pd) := 0
        INVARIANT ii : NAT & pd : ran(prod_alt_arr)
        VARIANT ii
      END
     END
  END;


  dd <-- pedirDesconto(cc) =
    dd := 0;


  entrarFila(cc) =
  BEGIN
    fila_r(hwm_fila) := cc;
    hwm_fila := hwm_fila + 1 //acho que isso tá errado
  END;


  sairFila(cc) =
  BEGIN
    VAR ii, fim IN
      ii := 0;
      fim := FALSE;
      WHILE ii < hwm_fila & fim = FALSE
      DO
        VAR ee IN
          ee := fila_r(ii);
          IF ee = cc
          THEN 
            fim := TRUE;
            VAR jj IN
              jj := ii;
              WHILE jj < hwm_fila-1
              DO
                fila_r(jj) := fila_r(jj+1);
                jj := jj + 1
              INVARIANT jj : ii..(hwm_fila-1)//jj < hwm_fila-1 & jj : NAT
              VARIANT (hwm_fila-1) - jj
              END;
              fila_r(hwm_fila-1) := ee
            END
          END;
          ii := ii+1
        END
      INVARIANT ii : 0..(hwm_fila-1) & fim = FALSE
      VARIANT hwm_fila - ii
      END;
      hwm_fila := hwm_fila - 1
    END
  END;


  qq <-- qtdClientesFila =
     BEGIN
         qq := card(ran(fila_r))
    END

 END